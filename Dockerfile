FROM mziescha/cpan-module-test:20170129

RUN         apt-get update && apt-get install -y gcc \
        &&  cpanm \
                DateTime \
                DateTime::Format::Builder \
        && apt-get remove --purge -y gcc && apt-get autoremove -y && apt-get clean && apt-get autoclean \
        && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/*

WORKDIR /app